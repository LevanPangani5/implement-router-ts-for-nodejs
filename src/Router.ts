import {IncomingMessage ,ServerResponse} from 'http';


type routs<T> = Record<string,T>

export class Router {
    private endpoints: routs<any>;
    constructor(){
        this.endpoints ={} as routs<any>;
    }
    get<T>(path: string, handler: Handler<T>): this {
         this.addEndpoint(HTTPMethod.GET, path,handler);
        return this;
    }

    post<T>(path: string, handler: Handler<T>): this {
        this.addEndpoint(HTTPMethod.POST, path,handler);
        return this;
    }

    put<T>(path: string, handler: Handler<T>): this {
        this.addEndpoint(HTTPMethod.PUT, path,handler);
        return this;
    }

    delete<T>(path: string, handler: Handler<T>): this {
        this.addEndpoint(HTTPMethod.DELETE, path,handler);
        return this;
    }

    getHandler() {
        return async (request: IncomingMessage&{body:object|undefined}, response: ServerResponse) => {
            const action:string|undefined = request.method;
            const path: string|undefined= request.url;
            const handler =this.endpoints[`${action}:${path}`];
            if(handler){
                try{
                    const req:IncomingMessage&{body:object|undefined} = await this.parseReq(request);
                    await handler(req,response);
                }catch(err){
                    response.statusCode=500;
                    response.end();
                }
            }
            else{
                response.statusCode=404;
                response.end();
            }
        }
    }

    private  addEndpoint<T>(action: HTTPMethod ,path: string, handler: Handler<T>){
        if(this.endpoints[`${action}:${path}`]){
            throw new Error(`Handler for this endpoint already exists`);
        }
        this.endpoints[`${action}:${path}`]=handler;
    }

    private async parseReq(req:IncomingMessage&{body:object|undefined}):Promise<IncomingMessage&{body:object|undefined}> {
    let data:string = '';

    req.on('data', (chunk:Buffer) => {
      data += chunk.toString('utf-8');
    });

    req.on('end', async() => {
      try {
        req.body = JSON.parse(data);
      } catch (error:unknown) {
        console.error('Error parsing request body:', error);
      }
    })
    return req;
}
}

export type Handler<T> = (request: IncomingMessage, response: ServerResponse) => void;

export enum HTTPMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE'
}

